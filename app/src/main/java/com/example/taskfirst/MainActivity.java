package com.example.taskfirst;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<Items> itemsList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        itemsList = new ArrayList<>();
        itemsList.add(new Items("Gardening","Description",R.drawable.ic_gardening));
        itemsList.add(new Items("Crops","Description",R.drawable.ic_crops));
        itemsList.add(new Items("Products","Description",R.drawable.ic_gardening));
        itemsList.add(new Items("Market Place","Description",R.drawable.ic_market_place));
        itemsList.add(new Items("Gallery","Description",R.drawable.ic_gallery));
        itemsList.add(new Items("Article","Description",R.drawable.ic_article));

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerID);
        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this, itemsList);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(myAdapter);


    }
}